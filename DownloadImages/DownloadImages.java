package net.rstvvoli.downloadImages;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DownloadImages {
    private static final String IN_FILE_TXT = "E:\\forproject\\downloadimages\\inFile.txt";
    private static final String OUT_FILE_TXT = "E:\\forproject\\downloadimages\\outFile.txt";
    private static final String PATH_TO_IMG = "E:\\forproject\\downloadimages\\images";

    public static void main(String[] args) {

        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {
            extractionOfLinksAndSave(inFile,outFile);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try (BufferedReader imageFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            downloadImg(imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод извлекает из вебстраниц ссылки на изображения из потока inFile
     * и сохраняет их в поток outFile;
     * @param inFile файл, из которого извлкчётся ссылка на сайт;
     * @param outFile файл, в который запишутся ссылки на изображения;
     * @throws IOException ошибка ввода-вывода;
     */
    private static void extractionOfLinksAndSave(BufferedReader inFile, BufferedWriter outFile) throws IOException {

        String link;
        while ((link = inFile.readLine()) != null) {
            URL url = new URL(link);
            String result;
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {

                result = bufferedReader.lines().collect(Collectors.joining("\n"));
            }
            Pattern imgLinkPattern = Pattern.compile("\\s*(?<=<img [^>]{0,100}src\\s?=\\s?\")[^>\"]+");
            Matcher matcher = imgLinkPattern.matcher(result);
            int i = 0;
            while (matcher.find() && i < 3) {
                outFile.write(matcher.group() + "\r\n");
                i++;
            }
        }
    }

    /**
     * Метод скачивает найденные изображения из файла;
     * @param imgFile файл, из которого необходимо скачать изображения;
     */
    private static void downloadImg(BufferedReader imgFile) {

        String img;
        int count = 0;
        try {
            while ((img = imgFile.readLine()) != null) {
                downloadUsingNIO(img, PATH_TO_IMG + String.valueOf(count) + ".jpg");
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод скачивает изображение из интерента по ссылке с помощью пакета java.nio;
     * @param strUrl ссылка на файл в интернете;
     * @param file путь к файлу, в который будет скачано содержимое из интернета;
     * @throws IOException ошибка ввода-вывода;
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {

        URL url = new URL(strUrl);
        /*
         * ReadableByteChannel - канал, способный только читать байты из файла;
         */
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        /*
         * getChannel() позволяет преобразовать поток вывода stream в файловый канал;
         * transferFrom() позволяет передать данные из byteChannel в файл,
         * к которому привязан fileChannel;
         * 0 - стартовая позиция для чтения;
         * Long.MAX_VALUE - количество прочитанных байт данных;
         * MAX_VALUE - максимальное значение, которое можно хранить в переменной типа long, означает что из источника мы прочитаем все байты;
         */
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }
}